module.exports = {
  mode: 'universal',

  head: {
    title: 'Сайт за спасибо',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, user-scalable=no' },
      { name: 'apple-mobile-web-app-title', content: 'zaSpasibo' },
      { name: 'application-name', content: 'zaSpasibo' },
      { name: 'msapplication-TileColor', content: '#2d89ef' },
      { name: 'msapplication-TileImage', content: '/mstile-144x144.png' },
      { name: 'theme-color', content: '#fff3f8' },
      { name: 'description', content: 'Некоммерческий проект, помогающий интересным людям с такими же интересными проектами получить свое место в сети абсолютно бесплатно' },
      { name: 'keywords', content: 'сайт за спасибо дизайн бесплатно профессионально качественно юрий костылев интересный проект благотворительность стартап сайт-визитка промо-страница онлайн-презентация каталог товаров веб-сайт веб-страница' },
      { name: 'msapplication-config', content: 'browserconfig.xml' },
      { name: 'msapplication-TileColor', content: '#fff3f8' },
      { name: 'theme-color', content: '#fff3f8' },
      { name: 'author', content: 'Юрий Костылев' },
      { name: 'twitter:card', content: 'product' },
      { name: 'twitter:creator', content: '@yourkostylev' },
      { name: 'twitter:title', content: 'Сайт за спасибо' },
      { name: 'twitter:description', content: 'Некоммерческий проект, помогающий интересным людям с такими же интересными проектами получить свое место в сети абсолютно бесплатно' },
      { name: 'twitter:label1', content: 'Цена' },
      { name: 'twitter:data1', content: '0 ₽' },
      { property: 'og:title', content: 'Сайт за спасибо' },
      { property: 'og:type', content: 'product' },
      { property: 'og:url', content: 'https://zaspasibo.yourkostylev.com/' },
      { property: 'og:image', content: 'http://zaspasibo.yourkostylev.com/images/banner@2x.jpg' },
      { property: 'og:image:secure_url', content: 'http://zaspasibo.yourkostylev.com/images/banner@2x.jpg' },
      { property: 'og:image:type', content: 'image/jpeg' },
      { property: 'og:image:width', content: '2000' },
      { property: 'og:image:height', content: '1300' },
      { property: 'og:image:alt', content: 'Cover' },
      { property: 'product:price:amount', content: '0' },
      { property: 'product:price:currency', content: 'RUB' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicon-16x16.png' },
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicon-32x32.png' },
      { rel: 'apple-touch-icon', sizes: '60x60', href: '/apple-touch-icon-60x60.png' },
      { rel: 'apple-touch-icon', sizes: '76x76', href: '/apple-touch-icon-76x76.png' },
      { rel: 'apple-touch-icon', sizes: '120x120', href: '/apple-touch-icon-120x120.png' },
      { rel: 'manifest', href: '/manifest.json' },
      { rel: 'mask-icon', href: '/safari-pinned-tab.svg', color: '#fc90c4' },
      { rel: 'preload', href: 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700&amp;subset=cyrillic', as: 'style', onload: 'this.rel="stylesheet"' }
    ],
    script: [
      {
        type: 'application/ld+json',
        src: JSON.stringify({
          '@context': 'http://schema.org/',
          '@type': 'Product',
          'brand': {
            '@type': 'Brand',
            'name': 'Юрий Костылев'
          },
          'name': 'Сайт за спасибо',
          'image': '/images/banner@2x.jpg',
          'description': 'Некоммерческий проект, помогающий интересным людям с такими же интересными проектами получить свое место в сети абсолютно бесплатно',
          'offers': {
            '@type': 'Offer',
            'priceCurrency': 'Rub',
            'price': '0.00',
            'itemCondition': 'new'
          }
        })
      }
    ]
  },

  loading: false,

  css: [
    'normalize.css/normalize.css'
  ],

  modules: [
    ['@nuxtjs/pwa', {
      icon: false,
      manifest: false,
      meta: false,
      onesignal: false
    }],
  ],

  build: {
    extend(config, ctx) {
      const svgRule = config.module.rules.find(rule => rule.test.test('.svg'))

      svgRule.test = /\.(png|jpe?g|gif|webp)$/

      config.module.rules.push({
        test: /\.svg$/,
        loader: 'vue-svg-loader',
        options: {
          svgo: false
        }
      })
    },
    postcss: [
      require('autoprefixer')({
        browsers: ['last 2 versions', 'ie >= 9']
      })
    ]
  },

  router: {
    scrollBehavior: async (to, from, savedPosition) => {
      if (savedPosition) {
        return savedPosition
      }

      const findEl = async (hash, x) => {
        return document.querySelector(hash) ||
          new Promise((resolve, reject) => {
            if (x > 50) {
              return resolve()
            }
            setTimeout(() => { resolve(findEl(hash, ++x || 1)) }, 100)
          })
      }

      if (to.hash) {
        let el = await findEl(to.hash)
        if ('scrollBehavior' in document.documentElement.style) {
          return window.scrollTo({ top: el.offsetTop, behavior: 'smooth' })
        } else {
          return window.scrollTo(0, el.offsetTop)
        }
      }

      return { x: 0, y: 0 }
    }
  }
}
