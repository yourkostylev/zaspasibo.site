<?php
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $_POST = json_decode(file_get_contents('php://input'), true);

    if(!empty($_POST)){
      if (!empty($_POST['message'])){

        if (!empty($_POST['name'])) {
          $name = strip_tags($_POST['name']);
          $nameFieldset = "Имя: ";
        }

        if (!empty($_POST['email'])) {
          $email = strip_tags($_POST['email']);
          $emailFieldset = "Email: ";
        }

        if (!empty($_POST['phone'])) {
          $phone = strip_tags($_POST['phone']);
          $phoneFieldset = "Телефон: ";
        }

        $message = strip_tags($_POST['message']);
        $messageFieldset = "Сообщение:%0A";

        $token = "493638063:AAGwuIsXI_4Ldw4aHQoPqvD3u2ZjdV5Y1Lk";
        $chat_id = "-227456329";
        $arr = array(
          $nameFieldset => $name,
          $phoneFieldset => $phone,
          $emailFieldset => $email,
          $messageFieldset => $message
        );

        foreach($arr as $key => $value) {
          $txt .= "<b>".$key."</b>".$value."%0A";
        };

        $sendToTelegram = fopen("https://api.telegram.org/bot{$token}/sendMessage?chat_id={$chat_id}&parse_mode=html&text={$txt}","r");

        if ($sendToTelegram) {
          echo 'Сообщение отправлено!';
          return true;
        } else {
          echo '<span class="form__error-text">Не удалось отправить, попробуйте еще раз</span>';
        }
      } else {
        echo '<span class="form__error-text">Вы не написали сообщение</span>';
      }
    }
  }
?>
